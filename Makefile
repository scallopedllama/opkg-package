# Simple Makefile for installing and packaging opkg-package

# Build target, does nothing
all:
	# Nothing to build

lint:
	shellcheck -x opkg-package
	shellcheck opkg-package-test

# Install target, installs to /usr/bin under DESTDIR
install:
	install -d                                ${DESTDIR}/usr/bin
	install -m 755 opkg-package               ${DESTDIR}/usr/bin
	install -d                                ${DESTDIR}/usr/lib/opkg-package/test
	install -m 644 opkg-package-test-lib.sh   ${DESTDIR}/usr/lib/opkg-package/test
	install -m 755 opkg-package-test          ${DESTDIR}/usr/lib/opkg-package/test
	cp      -r     test-deploy-root           ${DESTDIR}/usr/lib/opkg-package/test
	install -m 644 opkg-packageConfig.cmake   ${DESTDIR}/usr/lib/opkg-package

# Uninstall target, removes the files added by the install target
uninstall:
	rm     ${DESTDIR}/usr/bin/opkg-package
	rm -rf ${DESTDIR}/usr/lib/opkg-package

# Collect information for opkg target

# Package name is the repo name
PACKAGE=$(shell basename $(PWD))
# Version is obtained from git describe --tags or if there are no tags 0.0.$(number of commits in repo)
VERSION=$(shell git describe --tags 2> /dev/null)
ifeq ($(strip $(VERSION)),)
	VERSION=0.0.$(shell git rev-list --all --count)
endif
# Shell script so all architecture
ARCH=all
# Maintainer is the last person to make a commit in the repository
MAINTAINER=$(shell git log -1 --format="%aN <%aE>")
# Description of the project
DESCRIPTION=Utility that can be used for creating opkg-compatible packages of a build project tree.
# Section and Priority to match git-buildpackage
SECTION=vcs
PRIORITY=optional
# Source is the git remote for this project or this path if no git remote
SOURCE=$(shell git remote get-url origin 2> /dev/null)
ifeq ($(strip $(SOURCE)),)
	SOURCE=$(PWD)
endif
# Should be installed for user and group of root
OWNER=root
GROUP=root

# Temporary dir used by the opkg target
OPKG_BUILD_TMPDIR=/tmp/opkg-package-make-tmp

# opkg target to package opkg-package into an opkg package using opkg-package
opkg:
	DESTDIR="${OPKG_BUILD_TMPDIR}" make install
	./opkg-package -p "$(PACKAGE)" -v "$(VERSION)" -a "$(ARCH)" -m "$(MAINTAINER)" -d "$(DESCRIPTION)" \
	               -s "$(SECTION)" -P "$(PRIORITY)" -S "$(SOURCE)" -o "$(OWNER)" -g "$(GROUP)" "$(OPKG_BUILD_TMPDIR)"
	rm -rf "$(OPKG_BUILD_TMPDIR)"


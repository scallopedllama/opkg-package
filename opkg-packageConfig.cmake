### @file OpkgPackage.cmake
###
### Provided by the opkg-package project, this CMake file provides a function
### for setting up an opkg-package target to any given project.
###
###

### @brief Checks if the project source is within a git repository and sets OPKG_PACKAGE_IS_GIT_REPO to 1 if it is
###
### Run git status in the CMAKE_CURRENT_SOURCE_DIR and check its return value.
### If git status fails, set OPKG_PACKAGE_IS_GIT_REPO to 0, otherwise set it to 1.
function(OPKG_PACKAGE_IS_GIT_REPO)
	# Make sure git is installed
	find_program (GIT_PATH git)
	if (NOT GIT_PATH)
		set(OPKG_PACKAGE_IS_GIT_REPO 0 PARENT_SCOPE)
	endif()
	# Make sure this is a git repository
	exec_program(${GIT_PATH} ${CMAKE_CURRENT_SOURCE_DIR}
				ARGS            status
				OUTPUT_VARIABLE GIT_STATUS_OUTPUT
				RETURN_VALUE    GIT_STATUS_RETURN)
	if (GIT_STATUS_RETURN)
		set(OPKG_PACKAGE_IS_GIT_REPO 0 PARENT_SCOPE)
	endif ()
	
	set (OPKG_PACKAGE_IS_GIT_REPO 1 PARENT_SCOPE)
endfunction()

### @brief Add an `opkg-package` target to generate opkg packages for the project
###
### This function will add an `opkg-package` target to the project that generates
### the package set for the project.
### The target will run the install target, installing all project files to a custom deploy
### directory, then run opkg-package on that directory to package up the files, as configured
### with arguments.
###
### Arguments to opkg-package are provided CMake-style to this function.
###
### The following arguments must always be provided:
###      DESCRIPTION    - A brief description of the project
###      SECTION        - Package group for the project, typical examples include: admin, base, comm, editors, extras, games, graphics, kernel, libs, misc, net, text, web, and x11
###      PRIORITY       - Project package importance, can be one of: required, important, standard, optional, or extra
###
### The following arguments are detected using Git or CMake but can be overridden if specified:
###      PACKAGE            - Package name, defaults to CMake Project name
###      VERSION            - Package version, defaults to `git describe --tags` ouput or `0.0.${number of commits in repository}`
###      ARCHITECTURE       - Package architecture, defaults to architecture specified by `-march` in C / C++ flags or uses CMAKE_HOST_SYSTEM_PROCESSOR if not specified. Use "all" if not-platform-specific.
###      MAINTAINER         - Name and email of the person maintaining the package, defaults to usingthe name and email from the most recent commit in `git log`. Should be in format "John Smith <jsmith@example.com".
###      SOURCE             - Online location that the compiled source came from, defaults to using the remote `origin` URL from Git
### For these arguments, if the project is not in Git, they must be specified in the function call or it will fail.
### If project is in Git, if any of the arguments above are specified autodetection will be skipped and the provided value will be used instead.
###
### The following arguments are optional:
###      DEPENDENCIES       - List of dependencies for the generated packages, defaults to no dependencies.
###      PREINST            - Script to be run before installing main package, defaults to no pre-install  script
###      POSTINST           - Script to be run after  installing main package, defaults to no post-install script
###      PRERM              - Script to be run before removing main package,   defaults to no pre-removal  script
###      POSTRM             - Script to be run after  removing main package,   defaults to no post-removal script
###      OWNER              - Owner to use for files in package, defaults to root owner for all files. Set to "-" to use files' current owner
###      GROUP              - Group to use for files in package, defaults to root group for all files. Set to "-" to use files' current group
###
### The following arguments can take multiple options after:
###      ADDITIONAL_FIELDS  - Additional fields to include in the Opkg CONTROL file in the format `key=value`
###      ALLOW_EMPTY        - Packages that are allowed to be empty, named as opkg-package would generate them like `example`, `example-dev`, `example-staticdev`, etc.
function(SETUP_TARGET_FOR_OPKG_PACKAGE)
	# Parse arguments
	set(optional_args      "")
	set(one_value_args     PACKAGE VERSION ARCHITECTURE MAINTAINER DESCRIPTION SECTION PRIORITY SOURCE DEPENDENCIES PREINST POSTINST PRERM POSTRM OWNER GROUP)
	set(multi_value_args   ADDITIONAL_FIELDS ALLOW_EMPTY)
	cmake_parse_arguments(arg "${optional_args}" "${one_value_args}" "${multi_value_args}" "${ARGN}")
	
	# Check dependencies: need opkg-package
	find_program (OPKG_PACKAGE_PATH opkg-package)
	if (NOT OPKG_PACKAGE_PATH)
		message(FATAL_ERROR "opkg-package is used to create Opkg packages but was not found on this system.")
	endif ()
	
	# Check if project is in Git and that Git is installed, return stored in OPKG_PACKAGE_IS_GIT_REPO
	OPKG_PACKAGE_IS_GIT_REPO()
	
	
	
	# Handle old function arguments
	if (NOT "${arg_UNPARSED_ARGUMENTS}" STREQUAL "")
		list(GET arg_UNPARSED_ARGUMENTS 0 arg_DESCRIPTION)
		list(GET arg_UNPARSED_ARGUMENTS 1 arg_SECTION)
		list(GET arg_UNPARSED_ARGUMENTS 2 arg_PRIORITY)
		list(GET arg_UNPARSED_ARGUMENTS 3 arg_DEPENDENCIES)
		message(WARNING "opkg-package: SETUP_TARGET_FOR_OPKG_PACKAGE has been updated to use CMake-style arguments. Please update your CMakeLists.txt to use it." )
	endif()
	
	
	
	# Get Description
	if ("${arg_DESCRIPTION}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Package description not specified, add a DESCRIPTION argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS -d "${arg_DESCRIPTION}")
	
	
	# Get Section
	if ("${arg_SECTION}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Package section not specified, add a SECTION argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -s "${arg_SECTION}")
	
	
	# Get Priority
	if ("${arg_PRIORITY}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Package priority not specified, add a PRIORITY argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -P "${arg_PRIORITY}")
	
	
	
	
	
	# Get the Package name
	if ("${arg_PACKAGE}" STREQUAL "")
		# Not specified as argument to this function, use CMAKE_PROJECT_NAME
		set(arg_PACKAGE "${CMAKE_PROJECT_NAME}")
	endif()
	# Make sure that the variable got set to something
	if ("${arg_PACKAGE}" STREQUAL "")
		message (FATAL_ERROR "opkg-package: Package name not specified, either use the project() CMake function to set it or add a PACKAGE argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -p "${arg_PACKAGE}")
	
	
	
	# Get the package version
	if ("${arg_VERSION}" STREQUAL "" AND OPKG_PACKAGE_IS_GIT_REPO)
		# Not specified as an argument, try to get the version from Git.
		# Try to use git describe --tags
		exec_program(${GIT_PATH} ${CMAKE_CURRENT_SOURCE_DIR}
					ARGS            describe --tags
					OUTPUT_VARIABLE GIT_DESCRIBE_OUTPUT
					RETURN_VALUE    GIT_DESCRIBE_RETURN)
		# If git describe was able to describe the commit
		if (NOT GIT_DESCRIBE_RETURN)
			# Git describe was successful so just use its output unmodified
			# Opkg versioning works as you would expect with the output
			set(arg_VERSION "${GIT_DESCRIBE_OUTPUT}")
		else()
			# No tags defined, get number of commits in the repository
			exec_program(${GIT_PATH} ${CMAKE_CURRENT_SOURCE_DIR}
						ARGS            rev-list --all --count
						OUTPUT_VARIABLE GIT_COMMIT_COUNT_OUTPUT
						RETURN_VALUE    GIT_COMMIT_COUNT_RETURN)
			# If that command returned zero, use its output to set the version
			if (NOT GIT_COMMIT_COUNT_RETURN)
				# Format version string like 0.0.${GIT_COMMIT_COUNT_OUTPUT}
				set(arg_VERSION "0.0.${GIT_COMMIT_COUNT_OUTPUT}")
			endif()
		endif()
	endif()
	# Make sure that the variable got set to something
	if ("${arg_VERSION}" STREQUAL "")
			message(FATAL_ERROR "opkg-package: Could not determine a reasonable version string for project, Either tag a version in Git or add a VERSION argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -v "${arg_VERSION}")
	
	
	
	# Get the package architecture
	if ("${arg_ARCHITECTURE}" STREQUAL "")
		# Not specified as an argument, try to figure it out from the CMAKE_C_FLAGS and CMAKE_CXX_FLAGS
		set(TARGET_ARCH_REGEX "^.*-march[= ]([^ ]+).*$")
		string(REGEX MATCH "${TARGET_ARCH_REGEX}" TARGET_ARCH_MATCH "${CMAKE_C_FLAGS} ${CMAKE_CXX_FLAGS}")
		if (TARGET_ARCH_MATCH)
			string(REGEX REPLACE "${TARGET_ARCH_REGEX}" "\\1" TARGET_ARCH "${CMAKE_C_FLAGS} ${CMAKE_CXX_FLAGS}")
		else()
			set(TARGET_ARCH ${CMAKE_HOST_SYSTEM_PROCESSOR})
		endif()
		set(arg_ARCHITECTURE "${TARGET_ARCH}")
	endif()
	# Make sure that the variable got set to something
	if ("${arg_ARCHITECTURE}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Could not determine the package architecture. Add an ARCHITECTURE argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -a "${arg_ARCHITECTURE}")
	
	
	
	# Get the package maintainer
	if ("${arg_MAINTAINER}" STREQUAL "" AND OPKG_PACKAGE_IS_GIT_REPO)
		# Not specified as an argument
		
		# Get the Maintainer line from git - last commit's author
		exec_program(${GIT_PATH} ${CMAKE_CURRENT_SOURCE_DIR}
					ARGS            log -1 --format="%aN <%aE>"
					OUTPUT_VARIABLE OPKG_MAINTAINER_OUTPUT
					RETURN_VALUE    OPKG_MAINTAINER_RETURN)
		# Set the maintainer value if the git call worked
		if (NOT OPKG_MAINTAINER_RETURN)
			set(arg_MAINTAINER "${OPKG_MAINTAINER_OUTPUT}")
		endif ()
	endif()
	# Make sure that the variable got set to something
	if ("${arg_MAINTAINER}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Could not determine the package maintainer. Add a MAINTAINER argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE")
	endif()
	
	# Make sure that the maintainer value is of the correct format
	set(OPKG_MAINTAINER_REGEX "^.* <.*@.*>$")
	string(REGEX MATCH "${OPKG_MAINTAINER_REGEX}" OPKG_MAINTAINER_REGEX_MATCH "${arg_MAINTAINER}")
	if (NOT OPKG_MAINTAINER_REGEX_MATCH)
		message(FATAL_ERROR "opkg-package: MAINTAINER argument is in invalid format, it should be like `John Smith <jsmith@example.com>`")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -m "${arg_MAINTAINER}")
	
	
	
	
	# Get the source URL
	if ("${arg_SOURCE}" STREQUAL "" AND OPKG_PACKAGE_IS_GIT_REPO)
		# Not specified as an argument, get the source from git - pull url
		exec_program(${GIT_PATH} ${CMAKE_CURRENT_SOURCE_DIR}
					ARGS remote get-url origin
					OUTPUT_VARIABLE OPKG_SOURCE_OUTPUT
					RETURN_VALUE    OPKG_SOURCE_RETURN)
		if (NOT OPKG_SOURCE_RETURN)
			set(arg_SOURCE "${OPKG_SOURCE_OUTPUT}")
		endif()
	endif()
	# Make sure that the variable got set to something
	if ("${arg_SOURCE}" STREQUAL "")
		message(FATAL_ERROR "opkg-package: Could not determine the package source. Add a SOURCE argument to your call to SETUP_TARGET_FOR_OPKG_PACKAGE, use CMAKE_CURRENT_SOURCE_DIR if not online")
	endif()
	
	set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -S "${arg_SOURCE}")
	
	
	
	
	
	# Optional Arguments
	if (NOT "${arg_DEPENDENCIES}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -D "${arg_DEPENDENCIES}")
	endif()
	if (NOT "${arg_PREINST}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -i "${arg_PREINST}")
	endif()
	if (NOT "${arg_POSTINST}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -I "${arg_POSTINST}")
	endif()
	if (NOT "${arg_PRERM}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -r "${arg_PRERM}")
	endif()
	if (NOT "${arg_POSTRM}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -R "${arg_POSTRM}")
	endif()
	if ("${arg_OWNER}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -o "root")
	elseif (NOT "${arg_OWNER}" STREQUAL "-")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -o "${arg_OWNER}")
	endif()
	if ("${arg_GROUP}" STREQUAL "")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -g "root")
	elseif(NOT "${arg_GROUP}" STREQUAL "-")
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -g "${arg_GROUP}")
	endif()
	
	
	
	# Multiple option arguments
	foreach(field ${arg_ADDITIONAL_FIELDS})
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -A "${field}")
	endforeach()
	foreach(empty ${arg_ALLOW_EMPTY})
		set(OPKG_PACKAGE_PROGRAM_ARGUMENTS ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} -e "${empty}")
	endforeach()
	
	
	
	
	
	
	
	# Set a path to where the opkg package contents are being deployed
	set(OPKG_TARGET_DEPLOY_DIR ${CMAKE_CURRENT_BINARY_DIR}/opkg-package.deploy)
	
	add_custom_target(opkg-package
	                  COMMAND DESTDIR=${OPKG_TARGET_DEPLOY_DIR} ${CMAKE_COMMAND} --build . --target install
	                  COMMAND opkg-package ${OPKG_PACKAGE_PROGRAM_ARGUMENTS} "${OPKG_TARGET_DEPLOY_DIR}"
	                  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
	                  COMMENT Build OPKG package for ${PROJECT_NAME} VERBATIM
	)
endfunction()

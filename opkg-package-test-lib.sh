#!/bin/bash
# This file is a part of opkg-package
# opkg-package makes opkg-compatible packages of a build project tree
# Copyright (C) 2017 Joe Balough
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

### @file opkg-package-test-lib.sh
###
### This file is sourced by opkg-package when running the hidden unit-test feature.
### It is intended to be packaged separately from opkg-package so people who do not
### want the tests don't install them. It is also intended to not be runnable as a
### shell script as it does nothing.

### Runs a suite of unit tests for the opkg-packge utility
UnitTests()
{
	### Exit value for tests
	exitVal=0
	
	# Test the OpkgControlAddKeyValuePair / OpkgControlGetValue functions
	OpkgControlAddKeyValuePairExit=0
	echo "Test OpkgControlAddKeyValuePair..."
	
	# Add the test values to the structure
	TEST_VALUES="foo=bar baz=bin hello=goodbye"
	for i in ${TEST_VALUES}; do
		key="${i/=*/}"
		value="${i/*=/}"
		if ! OpkgControlAddKeyValuePair "${key}" "${value}"; then
			echo "     FAILURE: OpkgControlKeyValuePair failure: Cannot set key \"${key}\" to value \"${value}\"."
			OpkgControlAddKeyValuePairExit=1
		fi
	done
	
	# Make sure they can all be read out
	for i in ${TEST_VALUES}; do
		key="${i/=*/}"
		value="${i/*=/}"
		if ! readValue="$(OpkgControlGetValue "${key}")"; then
			echo "     FAILURE: OpkgControlKeyValuePair failure: Set ${key}=${value} but readback indicated failure."
			OpkgControlAddKeyValuePairExit=1
		fi
		if [ "${readValue}" != "${value}" ]; then
			echo "     FAILURE: OpkgControlKeyValuePair failure: Set ${key}=${value} but read \"${readValue}\" for that key."
			OpkgControlAddKeyValuePairExit=1
		fi
	done
	
	# Make sure that unset values return an empty string
	UNSET_TEST_VALUES="one two three"
	for i in ${UNSET_TEST_VALUES}; do
		readValue="$(OpkgControlGetValue "${i}")"
		if [ $? -ne 1 ]; then
			echo "     FAILURE: OpkgControlKeyValuePair failure: Did not set key ${key} but readback indicated success."
			OpkgControlAddKeyValuePairExit=1
		fi
		if [ "${readValue}" != "" ]; then
			echo "     FAILURE: OpkgControlKeyValuePair failure: Did not set key ${key} but read a non-empty value from that key."
			OpkgControlAddKeyValuePairExit=1
		fi
	done
	
	# Make sure values are overwritten when added a second time
	if ! OpkgControlAddKeyValuePair "foo" "foo"; then
		echo "     FAILURE: OpkgControlKeyValuePair failure: Cannot set key \"foo\" to value \"foo\"."
		OpkgControlAddKeyValuePairExit=1
	fi
	if ! readValue="$(OpkgControlGetValue "foo")"; then
		echo "     FAILURE: OpkgControlKeyValuePair failure: Set key foo but readback indicated failure."
		OpkgControlAddKeyValuePairExit=1
	fi
	if [ "${readValue}" != "foo" ]; then
		echo "     FAILURE: OpkgControlKeyValuePair failure: Set foo=foo but read \"${readValue}\" that key."
		OpkgControlAddKeyValuePairExit=1
	fi
	
	
	# Print pass if everything was OK
	if [ ${OpkgControlAddKeyValuePairExit} -eq 0 ]; then
		echo "     PASS"
	else
		exitVal=1
	fi
	
	
	
	
	# Test the CheckScript function
	CheckScriptExit=0
	echo "Test CheckScript..."
	
	# Pick a place there is probably not a file
	NON_EXIST_FILE_PATH="/path/to/script/that/definitely/does/not/exist/is/here/hooray"
	# Just to be sure, throw characters on the end of the path until we get one that does not exist
	while [ -e "${NON_EXIST_FILE_PATH}" ]; do
		NON_EXIST_FILE_PATH="${NON_EXIST_FILE_PATH}1"
	done
	# If CheckScript passes on this then that's a problem
	if CheckScript "${NON_EXIST_FILE_PATH}"; then
		echo "     FAILURE: CheckScript failure: CheckScript returned OK on a file that does not exist"
		CheckScriptExit=1
	fi
	
	# Test file exists but is not executable
	EXIST_BUT_NOT_EXEC_FILE_PATH="${OPKG_PACKAGE_TEMP_DIR}/testfile"
	rm "${EXIST_BUT_NOT_EXEC_FILE_PATH}" 2> /dev/null
	touch "${EXIST_BUT_NOT_EXEC_FILE_PATH}"
	if CheckScript "${EXIST_BUT_NOT_EXEC_FILE_PATH}"; then
		echo "     FAILURE: CheckScript failure: CheckScript returned OK on a script that is not executable"
		CheckScriptExit=1
	fi
	
	# Test all ok
	chmod +x "${EXIST_BUT_NOT_EXEC_FILE_PATH}"
	if ! CheckScript "${EXIST_BUT_NOT_EXEC_FILE_PATH}"; then
		echo "     FAILURE: CheckScript failure: CheckScript returned Failure on a script that should be OK"
		CheckScriptExit=1
	fi
	
	
	# Print pass if everything was OK
	if [ ${CheckScriptExit} -eq 0 ]; then
		echo "     PASS"
	else
		exitVal=1
	fi
	
	
	
	# Test Processing Command Line Arguments
	ProcessCommandLineArgsExit=0
	echo "Test ProcessCommandLineArgs..."
	
	# Run the command line arguments processing function with a bunch of arguments
	ProcessCommandLineArgs -p pkg -v ver -a arch -m maintainer -d desc -s sect -P priority -S source -D depends\
	                       -A one=two -A foo=bar -i preinst -I postinst -r prerm -R postrm \
	                       -o owner -g group -O output root_path
	
	# Check OPKG Control values
	TEST_VALUES="Package=pkg Version=ver Architecture=arch Maintainer=maintainer Description=desc Section=sect Priority=priority Source=source Depends=depends one=two foo=bar"
	for i in ${TEST_VALUES}; do
		key="${i/=*/}"
		value="${i/*=/}"
		readValue="$(OpkgControlGetValue "${key}")"
		if [ "${readValue}" != "${value}" ]; then
			echo "     FAILURE: ProcessCommandLineArgs failure: Expected \"${key}\" = \"${value}\" but got \"${readValue}\"."
			ProcessCommandLineArgsExit=1
		fi
	done
	
	# Check the scripts
	if [ "${OPKG_PACKAGE_PREINST}" != "preinst" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected preinst script to be \"preinst\" but it is set to \"${OPKG_PACKAGE_PREINST}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_PACKAGE_POSTINST}" != "postinst" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected postinst script to be \"postinst\" but it is set to \"${OPKG_PACKAGE_POSTINST}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_PACKAGE_PRERM}" != "prerm" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected prerm script to be \"prerm\" but it is set to \"${OPKG_PACKAGE_PRERM}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_PACKAGE_POSTRM}" != "postrm" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected postrm script to be \"postrm\" but it is set to \"${OPKG_PACKAGE_POSTRM}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_PACKAGE_FILE_OWNER}" != "--owner=owner" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected owner to be \"--owner=owner\" but it is set to \"${OPKG_PACKAGE_FILE_OWNER}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_PACKAGE_FILE_GROUP}" != "--group=group" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected group to be \"--group=group\" but it is set to \"${OPKG_PACKAGE_FILE_GROUP}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${OPKG_OUTPUT_DIR}" != "output" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected output dir to be \"output\" but it is set to \"${OPKG_OUTPUT_DIR}\"."
		ProcessCommandLineArgsExit=1
	fi
	if [ "${PACKAGE_ROOT_PATH}" != "root_path" ]; then
		echo "     FAILURE: ProcessCommandLineArgs failure: Expected package root to be \"root_path\" but it is set to \"${PACKAGE_ROOT_PATH}\"."
		ProcessCommandLineArgsExit=1
	fi
	
	# Print pass if everything was OK
	if [ ${ProcessCommandLineArgsExit} -eq 0 ]; then
		echo "     PASS"
	else
		exitVal=1
	fi
	
	
	
	
	
	exit ${exitVal}
}

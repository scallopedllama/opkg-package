# opkg-package - Does all the hard work for you when making opkg packages.

[![build status](https://gitlab.com/scallopedllama/opkg-package/badges/master/build.svg)](https://gitlab.com/scallopedllama/opkg-package/commits/master)

opkg-package is a utility that can be used for creating opkg-compatible packages of a build project tree.
It generates the control file and archive for you so there is no need to manually create one.
This script also splits the built project tree into separate packages according to the Debian package guidelines.

## Difference from `opkg-utils`

opkg-package does the same things that the opkg-build tool from opkg-utils does but it does more.

With opkg-build, you must create the control file, structure the directories correctly and it will package
exactly what you have in its folder, regardless of whether it makes sense or not.

opkg-package does all the packaging hard work for you.

Install the project you're trying to package to a deploy directory and have opkg-package package it up for you.
The packages will be automatically split according to Debian packaging guidelines. Instead of having to provide
a control file, all the control fields that aren't automatic are simply provided as command line arguments.

opkg-package also comes with a CMake package that provides an opkg-package target for your CMake project, making
package creation during CI pipelines a snap.

## Usage

```
opkg-package -h
opkg-package -p (Package) -v (Version) -a (Architecture) -m (Maintainer) -d (Description) -s (Section) -P (Priority) -S (Source) [-D (Dependencies)]
             [-A (field=value)] [-i (preinst)] [-I (postinst)] [-r (prerm)] [-R (postrm)] [-o (owner)] [-g (group)] [-O (Output directory)] (Package Root Path)
```

The first eight arguments are to set control variables and are required:

* `-p (Package)`:      The name of the package being generated, can only contain the characters `[a-z0-9.+-]` (no underscores).
	* The Package field will be automatically modified for other packages to match their filename.
* `-v (Version)`:      The repository version of the package. Note that for libraries, the so version may not match the repository version.
* `-a (Architecture)`: Target Architecture for which the package is built. For example: `x86_64` or `armv5te`.
* `-m (Maintainer)`:   Should be your name and email in the format `John Doe <jdoe@example.com>`
* `-d (Description)`:  A human readable description of the package, may contain newlines.
* `-s (Section)`:      The package group to which this package belongs. Typical examples are: admin, base, comm, editors, extras, games, graphics, kernel, libs, misc, net, text, web, and x11
* `-P (Priority)`:     The perceived importance of the package, must be one of the following: required, important, standard, optional, or extra.
	* If you don't know which priority value you should be using, then use 'optional'
* `-S (Source)`:       The path to the source used to build this package. Should be URLs or paths to repositories, patches, or tar archives.

From there, the following optional arguments are also supported:

* `-D (Dependencies)`: List of comma-separated dependencies for package. For example: `libcurl5 (>=7.32.0), libfreetype6 (>=2.4.12)`
	* This field is not checked in any way so make sure you [format it properly](https://www.debian.org/doc/debian-policy/ch-relationships.html).
* `-A (field=value)`:  Additional fields to include the control file. This argument can be provided multiple times.
	* The "Filename" field is forbidden
* `-i (preinst)`:      Path to a script to run before installing the main package
* `-I (postinst)`:     Path to a script to run after installing the main package
* `-r (prerm)`:        Path to a script to run before removing the main package
* `-R (postrm)`:       Path to a script to run after removing the main package
* `-e (package)`:      Packages allowed to be empty, can be passed multiple times matching the generated package name (like `libfoo1` or `foo-dev`)
* `-o (owner)`:        Force the specified user for all files in the package
* `-g (group)`:        Force the specified group for all files in the package
	* In most use cases, you want to use `root` for both owner and group
* `-O (Output dir)`:   Directory into which the output package(s) should be put. Defaults to the current working directory.

After providing the arguments, pass a built project tree's root path. To generate one of these, typically you
build the project like normal and provide a value for the `DESTDIR` environment variable when running install:
```
   $ make DESTDIR=./deploy install
```
This will install the project into the deploy directory as though it were the root folder of a system.

As mentioned above, this tool will properly split the project install directory into the expected Debian-style packages.
The following common paths will be split accordingly:

* `packagename_version_architecture.ipk` will contain files installed to `/usr/bin`, `/usr/share` (except man paths), and `/etc`
* `libpackagename1_version_architecture.ipk` will contain versioned `.so` files installed to `/usr/lib`
    * Multiple shared libraries are handled appropriately
    * Shared libraries automatically get a minimal postinst to run `/sbin/ldconfig`
* `packagename-staticdev_version_architecture.ipk` will contain static libraries installed to `/usr/lib`
* `packagename-dev_version_architecture.ipk` will contain header installed to `/usr/include` and unversioned `.so` symlinks installed to `/usr/lib`
* `packagename-test_version_architecture.ipk` will contain test files installed to `/usr/lib/packagename/test`
	* The creation of this package depends on there being a folder called `test` somewhere in a folder within `/usr/lib/`
* `packagename-doc_version_architecture.ipk` will contain documentation files installed to `/usr/share/man` or `/usr/man`

## Sanity Checks

While building the package, a number of sanity checks are automatically performed.

* Files owned by users when not using `-o` and `-g`
* Leading `v` stripped from provided Version field (feel free to use output from `git describe --tags`)
* Post / Pre Install / Remove scripts all exist and are executable
* Files installed to /usr/local

## Package Splitting

Based on the location in the deploy directory, the files installed by a project are split into
potentially many separate packages.

* Versioned library so files generated and installed to `/usr/lib/` are split into separate lib packages
	* The unversioned files are included in the -dev package
* Any manpages installed to `/usr/man` or `/usr/share/man` are split into a -doc package
* Static libraries installed to `/usr/lib` are split into a -staticdev package
* Test files installed to a `test` folder somewhere under `/usr/lib/` are split into a -test package
* All files installed to `/usr/include` and anything remaining in `/usr/lib` are split into the -dev package

Any files present in the deploy directory that were not packaged are listed with a warning at the end
of the process. If they are in the proper place of the Linux filesystem, feel free to file a bug to get them
included in the proper locations.

If a project does not install any files to split into one of the packages listed above, that package is
not generated.

## Empty Packages

`opkg-package` will typically skip generation of packages that contain no files. Empty pacakges can be forced
by using the `-e (package name)` argument one or many times. The `(package name)` must be one of the package names
generated by `opkg-package`. For example, for a project called `mytool`, this could be `mytool`, `mytool-dev`,
`mytool-doc`, `mytool-staticdev`, etc.

Empty packages can be used to create meta packages that depend on other packages to pull in a suite of tools
with one command. To make one, pass all arguments to create a package that contains no files but has dependencies
on the packages it should bring in when installed.

## Dependencies

Dependencies provided on the command line are specified in the control file for the main package, -dev package,
and all shared library packages.

Additionally, the following dependencies are configured automatically:

* The -dev, -test, and main packages automatically depend on all lib packages
* The -staticdev package automatically depends on the -dev package
* The -doc package has no dependencies and always has 'all' for Architecture

## CMake

Installed with opkg-package is an opkg-package CMake package. This package can be used to easily
add an opkg-package target to a CMake project to generate the OPKG packages for that project.

Simplest example usage in top-level CMakeLists.txt:

```cmake
find_package(opkg-package)
setup_target_for_opkg_package(DESCRIPTION "My awesome program"
                              SECTION     "base"
                              PRIORITY    "required")
```

Simply find the opkg-package package, then call the `setup_target_for_opkg_package` function with at
least the description, section, and priority specified.
After configuring the project, simply run the opkg-package target to create the packages.

```
$ make opkg-package
```

The packages are output to CMAKE_CURRENT_BINARY_DIR.

Arguments to `setup_target_for_opkg_package` are provided CMake-style with a description of the
argument before its value. As mentioned above, `DESCRIPTION`, `SECTION`, and `PRIORITY` must always
be specified.

Additionally, the following arguments are required but the `setup_target_for_opkg_package` function
can automatically determine their values using CMake variables or calls to Git. If the detected values
are wrong or you aren't using Git, you must specify them manually.

* `PACKAGE` - The base name of the OPKG package to generate
	* Uses the CMake Project name if not specified
* `VERSION` - The repository version for the package
	* If not specified, `opkg-package` will use `git describe --tags` to get a version or, if no
	  tags are defined, it will count the number of commits in the history and format the version
	  like `0.0.${number_of_commits}`
	* If the source is not in a Git repository and this is not specified, configuration will fail
* `ARCHITECTURE` - The target architecture of the compiled code
	* If not specified, `opkg-package` checks the C++ and C compile flags to look for a `-march` argument
	  and uses the architecture specified there. If there is no `-march` argument there, it assumes the
	  program is being compiled for the host architecture.
* `MAINTAINER` - The person maintaining the OPKG package, must be in the format `John Smith <jsmith@example.com>`
	* If not specified, `opkg-package` will use the author of the most recent commit in the Git repository
	* If the source is not in a Git repository and this is not specified, configuration will fail
* `SOURCE` - The online location of the source being compiled into a package
	* If not specified, `opkg-package` will use the `origin` remote's URL
	* If the source is not in a Git repository and this is not specified, configuration will fail

Finally, the following additional optional arguments are available:

* `DEPENDENCIES` - Packages that the generated package depends upon
	* Format of the string after this argument should match what `opkg-package` expects
* `PREINST` - Script to be run before installing main package
	* Defaults to no pre-install script
* `POSTINST` - Script to be run after  installing main package
	* Defaults to no post-install script
* `PRERM` - Script to be run before removing main package
	* Defaults to no pre-removal script
* `POSTRM` - Script to be run after  removing main package
	* Defaults to no post-removal script
* `OWNER` - Owner to use for files in package
	* Defaults to root owner for all files
	* Set to "-" to use files' current owner
* `GROUP` - Group to use for files in package
	* Defaults to root group for all files
	* Set to "-" to use files' current group


## Tests

The opkg-package tool has an automatic test utility to make sure that it functions properly.
To run the unit tests, just run the `opkg-package-test` utility in the root of the repository.
If installed, the tests are installed to `/usr/lib/opkg-package/test/opkg-package/test` and can
be run from any working directory.

## Yo dawg, We heard you like OPKG...

So we packaged your opkg-package with opkg-package so you can package your OPKGs using a tool you
installed with OPKG.

The `opkg` target in the makefile will use the `opkg-package` script in the repository to generate
opkg packages to use opkg-package:

* `opkg-package_0.0.46_all.ipk` - Contains the main `opkg-package` script
	* Only this package is required to create packages
* `opkg-package-test_0.0.46_all.ipk` - Contains the opkg-package tests
* `opkg-package-dev_0.0.46_all.ipk` - Contains the CMake package

